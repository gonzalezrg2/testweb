import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private http: HttpClient) { }

  consultarEmpleados() {
    const url = `${environment.urlApi}empleado`;
    const resultado = this.http.get(url);
    return resultado;
  }

  eliminarEmpleado(id: number){
    const url = `${environment.urlApi}empleado/${id}`;
    const resultado = this.http.delete(url);
    return resultado;
  }
}
