import { Component, OnInit } from '@angular/core';
import { EmpleadoModel } from 'src/app/model/empleado-model';
import { EmpleadoService } from 'src/app/services/empleado.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lista-empleados',
  templateUrl: './lista-empleados.component.html',
  styleUrls: ['./lista-empleados.component.scss']
})
export class ListaEmpleadosComponent implements OnInit {
  listaEmpleados: EmpleadoModel[] = [];

  constructor(private empleadoService: EmpleadoService) { }

  ngOnInit(): void {
    this.consultarEmpleados();
  }

  consultarEmpleados() {
    this.empleadoService.consultarEmpleados().subscribe(
      (response: EmpleadoModel[]) => {
        this.listaEmpleados = response;
      },
      err => {
        console.log('Error al consultar empleados')
      },
      () => {

      }
    );
  }

  eliminarEmpleado(id: number) {
    swal.fire({
      title: '¿Está seguro de querer eliminar el registro?',
      text: '¡No podrá recuperar este registro!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.value) {
        this.empleadoService.eliminarEmpleado(id).subscribe(
          (response: any) => {
            console.log(response);
          },
          err => {
            console.log('Error al eliminar empleado')
          },
          () => {

          }
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {

      }
    })
  }

}
