export class EmpleadoModel {
    id: number;
    nombre: string;
    edad: number;
    fechaNacimiento: Date
}
